﻿using System;

namespace RandomNameSelectorLibrary.Models
{
    public class Person : IComparable<Person>
    {
        private string Name { get; set; }

        public Person(string name)
        {
            this.Name = name;
        }

        public string GetName()
        {
            return this.Name;
        }

        public int CompareTo(Person otherPerson)
        {
            return this.Name.CompareTo(otherPerson.GetName());
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
