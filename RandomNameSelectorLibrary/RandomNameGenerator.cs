﻿using RandomNameSelectorLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RandomNameSelectorLibrary
{
    public class RandomNameGenerator
    {
        public readonly List<Person> randomNames = new();

        public RandomNameGenerator()
        {
            randomNames.Add(new("Danny"));
            randomNames.Add(new("Steve"));
            randomNames.Add(new("Nick"));
        }

        public void AddName(string name)
        {
            randomNames.Add(new(name));
        }

        public string GetRandomName()
        {
            Random r = new();
            return randomNames[r.Next(randomNames.Count)].GetName();
        }
    }
}
