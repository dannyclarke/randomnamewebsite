using Microsoft.VisualStudio.TestTools.UnitTesting;
using RandomNameSelectorLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RandomNameSelectorTestProject
{
    [TestClass]
    public class PersonTests
    {
        [TestMethod]
        public void Test_Set_New_Person_Count()
        {
            List<Person> people = new()
            {
                new Person("Danny"),
                new Person("Nick"),
                new Person("Steve")
            };

            Assert.AreEqual(people.Count, 3);
        }

        [TestMethod]
        public void Test_Person_Collection_Sorting()
        {
            List<Person> peopleUnsorted = new();
            List<Person> peopleSorted = new();

            Person p1 = new("Danny");
            Person p2 = new("Nick");
            Person p3 = new("Steve");

            peopleUnsorted.Add(p3);
            peopleUnsorted.Add(p2);
            peopleUnsorted.Add(p1);

            peopleSorted.Add(p1);
            peopleSorted.Add(p2);
            peopleSorted.Add(p3);

            peopleUnsorted.Sort();

            Assert.IsTrue(peopleSorted.SequenceEqual(peopleUnsorted));
        }
    }
}
