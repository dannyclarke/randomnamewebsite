﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RandomNameSelectorLibrary;
using RandomNameSelectorLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RandomNameSelectorTestProject
{
    [TestClass]
    public class RandomNameGeneratorTests
    {

        [TestMethod]
        public void Check_Three_Names_Are_Generorated_On_Contruct()
        {
            RandomNameGenerator rng = new();

            Assert.AreEqual(rng.randomNames.Count, 3);
        }

        [TestMethod]
        public void Check_Add_Name()
        {
            RandomNameGenerator rng = new();
            string testName = "Test";
            rng.AddName(testName);

            Assert.AreEqual(rng.randomNames.Count, 4);

            bool nameFound = false;

            foreach (Person p in rng.randomNames)
            {
                if (p.GetName() == testName)
                    nameFound = true;
            }

            Assert.AreEqual(nameFound, true);
        }

        [TestMethod]
        public void Check_Random_Name_Genrator()
        {
            RandomNameGenerator rng = new();

            string randomName = rng.GetRandomName();

            Assert.IsTrue((randomName == "Danny" || randomName == "Steve" || randomName == "Nick"), "Failed");
        }
    }
}
